import requests
import json
import logging
from datetime import datetime, timedelta
from dateutil import parser, tz
import re
import argparse

#access_token = 'D5ByZUBBcerBKBoy9ypS'
access_token = 'JYXyxv8JN3zzpTkrAnte'

################################### TAG FUNCTIONS ###################################
def create_a_tag(tag_name, ref, annoatated_tag_message='', release_note=''):
   '''
   Create a tag directly in remote repository. If release_note is specified, a release
   with the same name will be created automatically based on this tag.

   :param tag_name: String, name of the tag. Please note that there tag_name must satisfy standard naming
                    as described here: https://git-scm.com/docs/git-check-ref-format#_description
   :param ref: String, create tag using commit SHA, another tag name, or branch name.
   :param annoatated_tag_message: String, creates annotated tag if this message is specified
   :param release_note: String, add release notes to the Git tag and store it in the GitLab database. Please
                        note that if release_note is specified, a release will automatically be created
                        based on this newly created tag

   :return: Dictionary, response from GitLab. If succeeded, info about this newly created tag will be displayed.
            Otherwise, an error message is sent back
   '''
   header = {'PRIVATE-TOKEN': access_token}

   try:
       logging.debug('Creating a new tag with name ' + tag_name + ' via GitLab tag API...')
       # post_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/repository/tags?tag_name=' + \
       #            tag_name+ '&ref='+ ref + '&message=' + annoatated_tag_message + '&release_description=' + release_note
       # resp = requests.post(post_url, headers=header, verify='sar-gitlab-certificate.cer')

       post_url = 'https://gitlab.com/api/v4/projects/15730395/repository/tags?tag_name=' + tag_name + \
                  '&ref=' + ref + '&message=' + annoatated_tag_message + '&release_description=' + release_note
       resp = requests.post(post_url, headers=header)

       logging.debug('Successfully created new tag with name ' + tag_name)
       resp = json.loads(resp.text)

   except requests.exceptions.SSLError as err:
       logging.debug('SSL Error.' + err)
       return None

   return resp


def delete_a_tag(tag_name):
   '''
   Delete a tag in remote GitLab repository. Please note that if there is a release associated with
   this deleted tag, that release will be deleted as well.

   :param tag_name: String, name of the tag to delete

   :return:
   '''
   header = {'PRIVATE-TOKEN': access_token}

   try:
      logging.debug('Deleting tag with name ' + tag_name + ' via GitLab tag API...')
      # delete_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/repository/tags' + tag_name
      # resp = requests.delete(delete_url, headers=header, verify='sar-gitlab-certificate.cer')

      delete_url = 'https://gitlab.com/api/v4/projects/15730395/repository/tags/' + tag_name
      resp = requests.delete(delete_url, headers=header)

      logging.debug('Successfully deleted new tag with name ' + tag_name)

   except requests.exceptions.SSLError as err:
      logging.debug('SSL Error.' + err)
      return

   return


def get_a_tag(tag_name):
   '''
   Get info about a tag based on tag name.
   Example response is as follow: https://docs.gitlab.com/ee/api/tags.html#get-a-single-repository-tag

   :param tag_name: String, name of the tag to get details

   :return: A dictionary contains info about queried tag
   '''
   header = {'PRIVATE-TOKEN': access_token}

   try:
      logging.debug('Getting detailed info about a tag with name ' + tag_name + ' via GitLab tag API...')
      # get_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/repository/tags' + tag_name
      # resp = requests.delete(get_url, headers=header, verify='sar-gitlab-certificate.cer')

      get_url = 'https://gitlab.com/api/v4/projects/15730395/repository/tags/' + tag_name
      resp = requests.get(get_url, headers=header)

      logging.debug('Successfully queried info about tag ' + tag_name)
      resp = json.loads(resp.text)

      return resp

   except requests.exceptions.SSLError as err:
      logging.debug('SSL Error.' + err)
      return None


def find_latest_tag(branch_name):
   '''
   Find latest tag on a branch. Please note that since all tags follow this standard release process,
   latest is determined by name of the tag (i.e. v1.2.3 should be created closer in time than v1.2.2)

   :param branch_name: String, name of the branch
   :return: String, name of this latest tag on target branch
   '''
   header = {'PRIVATE-TOKEN': access_token}

   try:
      logging.debug('Get a list of tags on branch ' + branch_name + ' via GitLab tag API...')
      # post_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/repository/tags' + tag_name
      # resp = requests.delete(delete_url, headers=header, verify='sar-gitlab-certificate.cer')

      if branch_name == 'master':
         # get_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/repository/tags'
         get_url = 'https://gitlab.com/api/v4/projects/15730395/repository/tags'
      else:
         source_version, user_defined_name = extract_info_from_release_branch_name(branch_name)
         user_defined_name = user_defined_name.replace('release/' , '')
         source_version = source_version.replace('-beta', '')
         search_criteria = user_defined_name + '-' + source_version
         # get_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/repository/tags?search='\
         #           + '^' + search_criteria + '&order_by=name'
         get_url = 'https://gitlab.com/api/v4/projects/15730395/repository/tags?search=' \
                   + '^' + search_criteria + '&order_by=name'

      resp = requests.get(get_url, headers=header)

      logging.debug('Successfully queried tags on branch ' + branch_name)
      resp = json.loads(resp.text)

      if len(resp) > 0:
         latest_tag = resp[0]['name']
      else:
         # First tag on this branch, need to initialize tag to X.Y.Z.0
         latest_tag = None

      return latest_tag

   except requests.exceptions.SSLError as err:
      logging.debug('SSL Error.' + err)
      return None


################################### RELEASE FUNCTIONS ###################################
def extract_version_number_from_tag(tag, release_branch=False):
   '''
   Given a tag name generated from this release process, this function will extract major, minor, patch number
   (, and update number for release branch) as integer

   :param tag: String, tag name to extract info from
   :param release_branch: Boolean, indicate whether this tag is on master or release branches
                           True -> Tag on release branches
                           False -> Tag on master
   :return: Integers, in the order of major_ver, minor_ver, patch_num (, update_num)
   '''
   if release_branch:
      # For release branch, its version number is expected to be in the form X.Y.Z.U where X, Y, Z should not be updated,
      # and only U increases with every push or merge
      version_only = re.search('v(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)\.(?P<update>0|[1-9]\d*)', tag)
   else:
      version_only = re.search('v(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)', tag)

   if version_only:
      version_numbers = version_only.groups()

      major_ver = int(version_numbers[0])
      minor_ver = int(version_numbers[1])
      patch_num = int(version_numbers[2])

      if release_branch:
         update_num = int(version_numbers[3])
         return major_ver, minor_ver, patch_num, update_num
      else:
         return major_ver, minor_ver, patch_num
   else:
      raise Exception("Unsupported tag name: " + tag)

def extract_info_from_release_branch_name(branch_name):
   '''
   For release branches, their branch name actually contains three parts as specified here:
                     (release/)(user specified name)_(source version number)
   This function will help to extract user specified name and source version number from a given
   branch name, as:
    1. In releases, we only want user specified name in our release name
    2. When pushing to a release branch for the first time, we can only get its proper initial
       release number based on this source version number in the branch name

   :param branch_name: String, name of the release branch

   :return: Strings, in the order of source version number, user specified name
   '''
   source_version = branch_name.split('_')[-1]
   user_defined_name = branch_name.replace('_' + source_version, '')
   return source_version, user_defined_name

def categorize_release_notes():
   '''
   This function will read from a log file [release_note.log] of commit messages generated in shell script and
   categorize those commits into following types:
      1. BREAKING CHANGE
      2. fix
      3. feat
      4. docs
   :return: Arrays of commit messages based on type
   '''
   breaking_change = []
   fix_notes = []
   feature_notes = []
   doc_notes = []

   with open('release_note.log', 'r', encoding='UTF-16') as file:
      line = file.readline()
      while line:
         if 'BREAKING CHANGE: ' in line:
            num = str(len(breaking_change) + 1)
            breaking_change.append(num + '. ' + line)
         elif 'fix: ' in line:
            num = str(len(fix_notes) + 1)
            fix_notes.append(num + '. ' + line)
         elif 'feat: ' in line:
            num = str(len(feature_notes) + 1)
            feature_notes.append(num + '. ' + line)
         elif 'docs: ' in line:
            num = str(len(doc_notes) + 1)
            doc_notes.append(num + '. ' + line)
         else:
            pass
         line = file.readline()
   return breaking_change, fix_notes, feature_notes, doc_notes


def generate_release_data_master(latest_tag):
   '''
   Generate release note and new release version for merge requests on master branch.
   Release note will be based on log file [release_note.log] generated from shell script,
   and it uses Markdown for styling. This function is also responsible for calculating
   new release version number based on commit messages type.

   For detailed rules of how different parts of version number are bumped, please referring to
   Magellan v2 Wiki page.

   :param latest_tag: String, latest tag on master, used to calculated new release number
   :return: Strings, in the order of new tag name, new release name, release note
   '''
   breaking_change, fix_notes, feature_notes, doc_notes = categorize_release_notes()
   if latest_tag is not None:
      major_ver, minor_ver, patch_num = extract_version_number_from_tag(latest_tag)
   else:
      major_ver = 0
      minor_ver = 1
      patch_num = 0

   if len(doc_notes) > 0 or len(fix_notes) > 0:
      if len(doc_notes) > 0:
         doc_notes = '### Documentation Updates:\n' + ''.join(doc_notes)
      else:
         doc_notes = ''
      if len(fix_notes) > 0:
         fix_notes = '### Bug Fixes:\n' + ''.join(fix_notes)
      else:
         fix_notes = ''
      patch_num += 1
   else:
      doc_notes = ''
      fix_notes = ''

   if len(feature_notes) > 0:
      feature_notes = '### Feature Updates:\n' + ''.join(feature_notes)
      minor_ver += 1
      patch_num = 0
   else:
      feature_notes = ''

   if len(breaking_change) > 0:
      breaking_change = '### Breaking Changes:\n' + ''.join(breaking_change)
      major_ver += 1
      minor_ver = 0
      patch_num = 0
   else:
      breaking_change = ''

   new_tag_name = 'v' + str(major_ver) + '.' + str(minor_ver) + '.' + str(patch_num) + '-beta'
   new_release_name = 'v' + str(major_ver) + '.' + str(minor_ver) + '.' + str(patch_num) + ' beta'
   release_note = '## RELEASE NOTE\n' + breaking_change + feature_notes + fix_notes + doc_notes

   return new_tag_name, new_release_name, release_note


def generate_release_data_branch(latest_tag, full_branch_name):
   '''
   Generate release note and new release version for pushes on release branches.
   Release note will be based on log file [release_note.log] generated from shell script,
   and it uses Markdown for styling. This function is also responsible for calculating
   new release version number based on commit messages type.

   For detailed rules of how different parts of version number are bumped, please referring to
   Magellan v2 Wiki page.

   :param latest_tag: String, latest tag on this branch, used to calculated new release number
   :param full_branch_name: String, name of the branch to have a new release

   :return: Strings, in the order of new tag name, new release name, release note
   '''
   breaking_change, fix_notes, feature_notes, doc_notes = categorize_release_notes()
   if latest_tag is not None:
      major_ver, minor_ver, patch_num, update_num = extract_version_number_from_tag(latest_tag, release_branch=True)
      source_version, branch_name = extract_info_from_release_branch_name(full_branch_name)
   else:
      source_version, branch_name = extract_info_from_release_branch_name(full_branch_name)
      major_ver, minor_ver, patch_num = extract_version_number_from_tag(source_version)
      update_num = 0

   if len(doc_notes) > 0:
      doc_notes = '### Documentation Updates:\n' + ''.join(doc_notes)
   else:
      doc_notes = ''

   if len(fix_notes) > 0:
         fix_notes = '### Bug Fixes:\n' + ''.join(fix_notes)
   else:
      fix_notes = ''

   if len(feature_notes) > 0:
      feature_notes = '### Feature Updates:\n' + ''.join(feature_notes)
   else:
      feature_notes = ''

   branch_name = branch_name.replace('release/', '')
   version_tag = 'v' + str(major_ver) + '.' + str(minor_ver) + '.' + str(patch_num) + '.' + str(update_num + 1)

   new_tag_name = branch_name + '-' + version_tag + '-beta'
   new_release_name = branch_name + ' ' + version_tag + ' beta'
   release_note = '## RELEASE NOTE\n' + feature_notes + fix_notes + doc_notes

   return new_tag_name, new_release_name, release_note

def create_new_release(tag_name, release_name, release_note):
   '''
   Create a new release based on a pre-exist tag. Although you can create a release directly
   using create_a_tag, but that only allows tag and release to have the same name. In order
   to create a new release with a different name from its corresponding tag name, you could
   call create_a_tag first without specifying release_note, then use this function to create
   a new release after.

   :param tag_name: String, tag name to create release from
   :param release_name: String, name of the new release
   :param release_note: String, release note

   :return: Dictionary, response from GitLab. If succeeded, info about this newly created release
            will be displayed. Otherwise, an error message is sent back
   '''
   headers = {'Content-Type': 'application/json', 'PRIVATE-TOKEN': access_token}
   release_details = {"name": release_name,
                      "tag_name": tag_name,
                      "description": release_note}
   data = json.dumps(release_details)

   try:
       logging.debug('Creating a new release with name ' + release_name + ' based on tag ' + tag_name +
                     ' via GitLab release API...')
       # post_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/releases'
       # resp = requests.post(post_url, headers=headers, data=data, verify='sar-gitlab-certificate.cer')

       post_url = 'https://gitlab.com/api/v4/projects/15730395/releases'
       resp = requests.post(post_url, headers=headers, data=data)
       resp = json.loads(resp.text)

       logging.debug('Successfully created the new release.')

   except requests.exceptions.SSLError as err:
       logging.debug('SSL Error.' + err)
       return None
   return resp


def get_release_list():
   '''
   Get a list of releases from a project, sorted by time at which each release is done

   :return: An array of dictionaries, each of which contains info about that release
   '''
   header = {'PRIVATE-TOKEN': access_token}

   try:
      logging.debug('Getting list of all releases via GitLab release API...')
      # get_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/releases'
      # resp = requests.delete(get_url, headers=header, verify='sar-gitlab-certificate.cer')

      get_url = 'https://gitlab.com/api/v4/projects/15730395/releases'
      resp = requests.get(get_url, headers=header)

      logging.debug('Successfully queried all release info')
      resp = json.loads(resp.text)

      return resp

   except requests.exceptions.SSLError as err:
      logging.debug('SSL Error.' + err)
      return None


def find_releases_since_last_nightly(branch_name):
   '''
   Get a list of releases since last nightly test. These releases will be updated
   from beta to formal release version after nightly tests pass.

   :param branch_name: String, name of the branch to get releases from
   :return: An array of dictionaries, each of which contains info about a release
            that might need to be updated
   '''
   time_now = datetime.utcnow()
   last_nightly = time_now - timedelta(days=1)
   release_list = get_release_list()

   to_return_list = []
   if branch_name == 'master':
      prefix = 'v'
   else:
      source_version, user_defined_name = extract_info_from_release_branch_name(branch_name)
      source_version = source_version.replace('-beta', '')
      user_defined_name = user_defined_name.replace('release/', '')
      prefix = user_defined_name + '-' + source_version

   for release_rec in release_list:
      tag_name = release_rec['tag_name']

      if tag_name.startswith(prefix):
         time_released = release_rec['released_at']

         # Covert all timestamps into utc time for comparison
         try:
            release_date = datetime.strptime(time_released, "%Y-%m-%dT%H:%M:%S.%fZ")
         except:
            release_date = parser.isoparse(time_released)
            release_date = release_date.astimezone(tz.tzutc())

         if release_date > last_nightly:
            to_return_list.append(release_rec)
         else:
            break

   return to_return_list

def update_beta_tag_and_release(branch_name):
   '''
   Wrapper function used to update all beta releases to formal releases after
   a nightly job succeeds.

   :param branch_name: String, name of the branch to update
   :return:
   '''
   release_list = find_releases_since_last_nightly(branch_name)

   for release_rec in reversed(release_list):
      tag_name = release_rec['tag_name']
      if '-beta' in tag_name:
         try:
            delete_a_tag(tag_name)
         except:
            logging.debug('Deletion of tag ' + tag_name + 'failed.')
            continue

         new_tag_name = tag_name.replace('-beta', '')

         if branch_name == 'master':
            new_release_name = new_tag_name
         else:
            version_num = new_tag_name.split('-')[-1]
            new_release_name = new_tag_name.replace('-' + version_num, '')
            new_release_name += " " + version_num

         ref = release_rec["commit"]['id']
         release_note = release_rec["description"]

         try:
            annotated_tag_message = release_rec["commit"]["message"]
         except:
            annotated_tag_message = ''

         try:
            create_a_tag(tag_name=new_tag_name, ref=ref, annoatated_tag_message=annotated_tag_message)
            create_new_release(tag_name=new_tag_name, release_name=new_release_name, release_note=release_note)
         except:
            logging.debug('Updating of beta tag ' + tag_name + 'failed.')
            continue


def release_process(target_branch_name, tag_message=''):
   '''
   Wrapper function used to generate a release on a branch

   :param target_branch_name: String, name of the branch to generate release on
   :param tag_message: (Optional) String, message goes to the tag

   :return:
   '''
   latest_tag = find_latest_tag(target_branch_name)
   if target_branch_name == 'master':
      new_tag_name, new_release_name, release_note = generate_release_data_master(latest_tag)
   else:
      new_tag_name, new_release_name, release_note = generate_release_data_branch(latest_tag, target_branch_name)

   resp_tag_creation = create_a_tag(tag_name=new_tag_name, ref=target_branch_name, annoatated_tag_message=tag_message)
   resp_release_creation = create_new_release(tag_name=new_tag_name, release_name=new_release_name, release_note=release_note)
   logging.debug("Response from create_a_tag in release_process: " + str(resp_tag_creation))
   logging.debug("Response from create_new_release in release_process: " + str(resp_release_creation))


if __name__ == "__main__":
   LOG_FILENAME = 'example.log'
   logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)
   logger = logging.getLogger(__name__)

   parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                    description='Magellan v2 release process on GitLab, \
                                                 support create a new release process\
                                                or update existing beta ones.')

   parser.add_argument('-n', dest='new_release_branch', action='store', type=str,
                       help='Create a new release on a given branch')
   parser.add_argument('-u', dest='branch_to_update', action='store', type=str,
                       help='Update existing beta releases on a branch since last nightly job.')

   args = parser.parse_args()

   if args.new_release_branch is not None:
      release_process(args.new_release_branch)
   if args.branch_to_update is not None:
      update_beta_tag_and_release(args.branch_to_update)


