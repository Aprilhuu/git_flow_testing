import coverage
import json
import argparse
import unittest
from tst.gitlab_ci.gitlab_ci_interface import activate_deactivate_nightly_schedule

def class_for_name(module_name, class_name):
   # load the module, will raise ImportError if module cannot be loaded
   m = __import__(module_name, globals(), locals(), class_name)
   # get the class, will raise AttributeError if class cannot be found
   c = getattr(m, class_name)
   return c

if __name__ == '__main__':
   # read json file and load regression or smoke test cases
   suiteFilePath = 'testSuite.json'
   with open(suiteFilePath, 'r') as f:
       testSuite = json.loads(f.read())
   acceptedModes = list(testSuite)
   acceptedModes.append('all')

   parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                    description='Magellan Unit test runner, \
                                    can run all the tests in tst folder using discovery method \
                                    or run regression tests \
                                    or run smoke tests')

   parser.add_argument('mode',
                       help="Testing level",
                       choices=acceptedModes)

   args = parser.parse_args()

   loader = unittest.TestLoader()

   if args.mode == 'all':
       tests = loader.discover('../')
       runner = unittest.TextTestRunner(verbosity=2)
       result = runner.run(tests)

   elif args.mode == 'gitlab_ci_nightly':
       cov = coverage.Coverage()
       cov.start()

       tests = loader.discover('../')
       runner = unittest.TextTestRunner(verbosity=2)
       result = runner.run(tests)

       cov.stop()
       cov.save()

       try:
           coverage_score = cov.report()
           cov.html_report()
           activate_deactivate_nightly_schedule(deactive=True)
           assert coverage_score > 90, "Coverage under 90%. Coverage test failed."
       except coverage.misc.CoverageException:
           print("Coverage.py warning: No data was collected. (no-data-collected)")

   else:
       testSuiteToRun = testSuite[args.mode]
       suite = unittest.TestSuite()
       testCaseList = {}

       for currTest in testSuiteToRun:
           """ we can have the complete path in the JSON, and then use split to get the last part as test class name 
               and rest of it as module name """
           # split the currTest to get the module and test class name
           currTestString = currTest.encode('utf8')
           module, testClass = str.split(currTestString, '.')
           tests = loader.loadTestsFromTestCase(class_for_name(module, testClass))
           testCaseList[currTestString] = loader.getTestCaseNames(class_for_name(module, testClass))
           suite.addTests(tests)

       runner = unittest.TextTestRunner(verbosity=2)
       count = suite.countTestCases()

       result = runner.run(suite)

       if args.mode == 'gitlab_ci_non_blocking':
          activate_deactivate_nightly_schedule(deactive=False)


