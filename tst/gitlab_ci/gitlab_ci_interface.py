import requests
import json
import logging
import argparse

#access_token = 'D5ByZUBBcerBKBoy9ypS'
access_token = 'JYXyxv8JN3zzpTkrAnte'


def activate_deactivate_nightly_schedule(deactive=False):
   header = {'PRIVATE-TOKEN': access_token}
   if deactive:
      form = {"active": "false"}
   else:
      form = {"active": "true"}

   try:
      logging.debug('Getting branch info via GitLab commit API...')
      # post_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/pipeline_schedules/3'
      # resp = requests.post(post_url, headers=header, data=form, verify='sar-gitlab-certificate.cer')

      post_url = 'https://gitlab.com/api/v4/projects/15530812/pipeline_schedules/42178'
      resp = requests.put(post_url, headers=header, data=form)

      logging.debug('Successfully queried branch job info')
      resp = json.loads(resp.text)
      return resp

   except requests.exceptions.SSLError as err:
      logging.debug('SSL Error.' + err)
      return None


def find_src_branch_of_merge_request(target_branch, CI_commit_sha):
   header = {'PRIVATE-TOKEN': access_token}

   try:
      logging.debug('Getting branch info via GitLab commit API...')
      # post_url = 'https://sar-gitlab.qualcomm.com/api/v4/projects/133/merge_requests?state=merged' \
      #            '&&order_by=updated_at&&target_branch='+target_branch
      # resp = requests.get(post_url, headers=header, verify='sar-gitlab-certificate.cer')

      post_url = 'https://gitlab.com/api/v4/projects/15730395/merge_requests?state=merged&&order_by=updated_at&&target_branch='+target_branch
      resp = requests.get(post_url, headers=header)

      resp = json.loads(resp.text)
      logging.debug('Successfully queried branch job info')

      if len(resp) > 0:
         for merge_rec in resp:
            if merge_rec['merge_commit_sha'] == CI_commit_sha:
               return merge_rec['source_branch']
            else:
               continue
         return None
      else:
         return None

   except requests.exceptions.SSLError as err:
      logging.debug('SSL Error.' + err)
      return None

if __name__ == "__main__":
   parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                    description='Magellan v2 & GitLab-ci Interface, \
                                                 support various functionality associated \
                                                 with updating remote GitLab repo.')

   parser.add_argument('-f', dest='find_info', action='store', type=str, nargs=2,
                       help='Find source branch of latest merge request on a target branch. Please provide'
                            'SOURCE_BRANCH as the first argument and CI_COMMIT_SHA as the second one.')
   parser.add_argument('-d', dest='deactivate_nightly', action='store_true',
                       help='If specified, deactivate nightly on a branch')
   parser.add_argument('-a', dest='activate_nightly', action='store_true',
                       help='If specified, activate nightly on a branch')

   args = parser.parse_args()

   if args.find_info is not None:
      source_branch_name = find_src_branch_of_merge_request(args.find_info[0], args.find_info[1])
      print(source_branch_name)
   if args.deactivate_nightly:
      activate_deactivate_nightly_schedule(deactive=True)
   if args.activate_nightly:
      activate_deactivate_nightly_schedule(deactive=False)
