#!/usr/bin/env bash

# Default behaviour: Checkout a tag and not create a new branch for it
as_new_branch=false

while [ $# -gt 0 ]
do
    case $1 in
    -n|--new) as_new_branch=true ;
              branch_name="$2" ; shift;;
    -t|--tag) tag_name="$2" ; shift;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2;
         echo "Supported options are:";
         echo " -n/--new: If specified, checkout tag as a new branch";
         echo " -t/--tag: Tag to checkout";
         exit 1;;
    (*) break;;
    esac
    shift
done

valid_branch_regex="^(feature|release)\/[a-z0-9._-]+$"
message="There is something wrong with your branch name. Branch names in this project must adhere to this contract:
         $valid_branch_regex.
Nothing is checked out. You should rename your branch to a valid name and try again."

if [[ ! $branch_name =~ $valid_branch_regex ]]
then
    echo "$message"
    exit 1
fi

# Fetch all tags before checkout
git fetch --tags

if [[ "$as_new_branch" = true ]]; then
    if [[ -z "$branch_name" ]]; then
        # Branch name is required when checkout as new branch
        echo Branch name not provided!;
    else
        git checkout tags/"$tag_name" -b "${branch_name}_${tag_name}";
    fi;
else
   git checkout tags/"$tag_name";
fi;