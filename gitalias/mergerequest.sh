#!/usr/bin/env bash

while [ $# -gt 0 ]
do
    case $1 in
    # for options with required arguments, an additional shift is required
    -n|--name) branch_name="$2" ; shift;;
    -t|--title) mr_title="$2" ; shift;;
    -d|--description) mr_description="$2" ; shift;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2;
         echo "Supported options are:";
         echo " -n/--name: Target branch name";
         echo " -t/--title: Merge request title";
         echo " -d/--description: Merge request description";
         exit 1;;
    (*) break;;
    esac
    shift
done

# Merging to master branch by default
if [[ -z "$branch_name" ]]; then
        echo ;
        echo No target branch name provided, using master...;
        branch_name="master";
fi;

if [[ -z "$mr_title" ]]; then
        # Merge request title is requested
        echo Merge request title not provided!;
elif [[ -z "$mr_description" ]]; then
        # Merge request description is requested
        echo Merge request description not provided!;
else
    echo ;
    echo Intializing merge request...;
    echo Target Branch: "$branch_name";
    echo Request Title: "$mr_title";
    echo Request Description: "$mr_description";
    echo ;
    git push -o merge_request.create -o merge_request.target="$branch_name" -o merge_request.title="$mr_title" -o merge_request.description="$mr_description";
fi;



